//
//  DashboardRouter.swift
//  StreamingApp
//
//  Created by José Ángel López on 23/02/22.
//

import Foundation
import UIKit

class DashboardRouter: PresenterToRouterDashboardProtocol {
    typealias SubModules = (shows: HomeViewController, favorites: HomeViewController)
    
    static func createDashboardScreen() -> DashboardViewController {
        let submodules = (shows: HomeRouter.createShowsScreen(), favorites: HomeRouter.createShowsScreen())
        let tabs = tabs(usingSubmodules: submodules)
        let view = DashboardViewController(nibName: Constants.XIBName.dashboard, bundle: nil)
        let presenter: ViewToPresenterDashboardProtocol & InteractorToPresenterDashboardProtocol = DashboardPresenter()
        let interactor: PresenterToInteractorDashboardProtocol = DashboardInteractor()
        let router: PresenterToRouterDashboardProtocol = DashboardRouter()
        
        view.dashboardPresenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        view.viewControllers = [tabs.shows, tabs.favorites]
        return view
    }
    
    static func tabs(usingSubmodules submodules: SubModules) -> DashboardTabs {
        let showsTabBarItem = UITabBarItem(title: Constants.tabBarTitles.shows, image: UIImage(named: Constants.tabBarImages.shows), tag: 0)
        let favoritesTabBarItem = UITabBarItem(title: Constants.tabBarTitles.favorites, image: UIImage(named: Constants.tabBarImages.favorites), tag: 1)
        submodules.shows.tabBarItem = showsTabBarItem
        submodules.shows.isHomeSelected = true
        submodules.favorites.tabBarItem = favoritesTabBarItem
        submodules.favorites.isHomeSelected = false
        return (shows: submodules.shows, favorites: submodules.favorites)
    }
}
