//
//  DashboardPresenter.swift
//  StreamingApp
//
//  Created by José Ángel López on 23/02/22.
//

import Foundation

class DashboardPresenter: ViewToPresenterDashboardProtocol {
    var view: PresenterToViewDashboardProtocol?
    var interactor: PresenterToInteractorDashboardProtocol?
    var router: PresenterToRouterDashboardProtocol?
}

// MARK: - ARCHITECTURE EXTENSION
extension DashboardPresenter: InteractorToPresenterDashboardProtocol {}
