//
//  DashboardViewController.swift
//  StreamingApp
//
//  Created by José Ángel López on 23/02/22.
//

import UIKit

typealias DashboardTabs = (shows: UIViewController, favorites: UIViewController)
class DashboardViewController: UITabBarController {
    // MARK: - VARIABLES
    var dashboardPresenter: ViewToPresenterDashboardProtocol?

    // MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewComponents()
    }
    
    // MARK: - SETUP VIEW COMPONENTS
    private func setupViewComponents() {
        setTitle()
        setTabBar()
    }
    
    private func setTitle() {
        overrideUserInterfaceStyle = .light
        self.title = Constants.navigationBarTitle.home
    }
    
    private func setTabBar() {
        self.tabBar.standardAppearance = tabBar.standardAppearance
        self.tabBar.scrollEdgeAppearance = tabBar.standardAppearance
        self.delegate = self
        self.tabBarController?.delegate = self
    }
}

// MARK: - ARCHITECTURE PROTOCOLS
extension DashboardViewController: PresenterToViewDashboardProtocol {}

// MARK: - TAB BAR DELEGATE METHODS
extension DashboardViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        switch tabBarController.selectedIndex {
        case 0:
            if  let controller = viewController as? HomeViewController {
                controller.showHomeData(true)
            }
        case 1:
            if let controller = viewController as? HomeViewController {
                controller.showHomeData(false)
            }
        default: return
        }
    }
}


