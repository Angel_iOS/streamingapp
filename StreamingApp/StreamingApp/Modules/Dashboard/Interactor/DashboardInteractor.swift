//
//  DashboardInteractor.swift
//  StreamingApp
//
//  Created by José Ángel López on 23/02/22.
//

import Foundation

class DashboardInteractor: PresenterToInteractorDashboardProtocol {
    var presenter: InteractorToPresenterDashboardProtocol?
}
