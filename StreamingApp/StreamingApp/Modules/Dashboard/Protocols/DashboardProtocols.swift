//
//  DashboardProtocols.swift
//  StreamingApp
//
//  Created by José Ángel López on 23/02/22.
//

import Foundation

// MARK: - ARCHITECTURE PROTOCOLS
protocol ViewToPresenterDashboardProtocol: AnyObject {
    var view: PresenterToViewDashboardProtocol? {get set}
    var interactor: PresenterToInteractorDashboardProtocol? {get set}
    var router: PresenterToRouterDashboardProtocol? {get set}
}

protocol PresenterToViewDashboardProtocol: AnyObject {}

protocol PresenterToRouterDashboardProtocol: AnyObject {}

protocol PresenterToInteractorDashboardProtocol: AnyObject {
    var presenter: InteractorToPresenterDashboardProtocol? {get set}
}

protocol InteractorToPresenterDashboardProtocol: AnyObject {}
