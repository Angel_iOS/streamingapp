//
//  DetailRouter.swift
//  StreamingApp
//
//  Created by José Ángel López on 01/03/22.
//

import Foundation

class DetailRouter: PresenterToRouterDetailProtocol {
    static func createDetailScreen(tvShowDetail: TVShowDetail) -> DetailViewController {
        let view = DetailViewController(nibName: Constants.XIBName.detail, bundle: nil)
        let presenter: ViewToPresenterDetailProtocol & InteractorToPresenterDetailProtocol = DetailPresenter()
        let interactor: PresenterToInteractorDetailProtocol = DetailInteractor()
        let router: PresenterToRouterDetailProtocol = DetailRouter()
        
        view.detailPresenter = presenter
        view.tvShowDetail = tvShowDetail
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        return view
    }
}
