//
//  Detail.swift
//  StreamingApp
//
//  Created by José Ángel López on 01/03/22.
//

import Foundation

struct TVShowDetail {
    let id: Int?
    let name: String?
    let image: String?
    let summary: String?
    let imdb: String?
}
