//
//  DetailPresenter.swift
//  StreamingApp
//
//  Created by José Ángel López on 01/03/22.
//

import Foundation

class DetailPresenter: ViewToPresenterDetailProtocol {
    var view: PresenterToViewDetailProtocol?
    var interactor: PresenterToInteractorDetailProtocol?
    var router: PresenterToRouterDetailProtocol?
}

// MARK: - ARCHITECTURE EXTENSION
extension DetailPresenter: InteractorToPresenterDetailProtocol {}

