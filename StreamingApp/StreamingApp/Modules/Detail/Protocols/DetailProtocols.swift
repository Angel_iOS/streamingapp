//
//  DetailPresenter.swift
//  StreamingApp
//
//  Created by José Ángel López on 01/03/22.
//

import Foundation
import UIKit.UINavigationController

// MARK: - ARCHITECTURE PROTOCOLS
protocol ViewToPresenterDetailProtocol: AnyObject {
    var view: PresenterToViewDetailProtocol? {get set}
    var interactor: PresenterToInteractorDetailProtocol? {get set}
    var router: PresenterToRouterDetailProtocol? {get set}
}

protocol PresenterToViewDetailProtocol: AnyObject {}

protocol PresenterToRouterDetailProtocol: AnyObject {}

protocol PresenterToInteractorDetailProtocol: AnyObject {
    var presenter: InteractorToPresenterDetailProtocol? {get set}
}

protocol InteractorToPresenterDetailProtocol: AnyObject {}
