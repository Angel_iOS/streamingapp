//
//  DetailInteractor.swift
//  StreamingApp
//
//  Created by José Ángel López on 01/03/22.
//

import Foundation

class DetailInteractor: PresenterToInteractorDetailProtocol {
    var presenter: InteractorToPresenterDetailProtocol?
}
