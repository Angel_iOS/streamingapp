//
//  DetailViewController.swift
//  StreamingApp
//
//  Created by José Ángel López on 01/03/22.
//

import UIKit

class DetailViewController: UIViewController {
    // MARK: - OUTLETS
    @IBOutlet weak var tvShowImage: UIImageView!
    @IBOutlet weak var tvShowSummary: UILabel!
    @IBOutlet weak var tvShowLink: UILabel!
    
    
    // MARK: - VARIABLES
    var detailPresenter: ViewToPresenterDetailProtocol?
    var tvShowDetail: TVShowDetail?
    
    // MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewComponents()
    }
    
    // MARK: - SETUP VIEW COMPONENTS
    private func setupViewComponents() {
        setNavigationBar()
        setImage()
        setSummaryLabel()
        setLink()
    }
    
    private func setNavigationBar() {
        overrideUserInterfaceStyle = .light
        self.title = tvShowDetail?.name
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    private func setImage() {
        guard let image = tvShowDetail?.image, !image.isEmpty else { return }
        self.tvShowImage.load(url: URL(string: image))
    }
    
    private func setSummaryLabel() {
        let summary = tvShowDetail?.summary ?? ""
        let summaryClean = summary.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        self.tvShowSummary.text = summaryClean
    }
    
    private func setLink() {
        tvShowLink.isUserInteractionEnabled = true
        tvShowLink.lineBreakMode = .byWordWrapping
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnLabel(_:)))
        tapGesture.numberOfTouchesRequired = 1
        tvShowLink.addGestureRecognizer(tapGesture)
    }
    
    // MARK: - USER INTERACTION
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        guard let imdb = tvShowDetail?.imdb, !imdb.isEmpty else {
            tvShowLink.isHidden = true
            return
        }
        if let url = URL(string: "https://www.imdb.com/title/\(imdb)") {
            UIApplication.shared.open(url)
        }
    }
}

// MARK: - ARCHITECTURE PROTOCOLS
extension DetailViewController: PresenterToViewDetailProtocol {}
