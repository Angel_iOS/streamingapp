//
//  HomeProtocols.swift
//  StreamingApp
//
//  Created by José Ángel López on 23/02/22.
//

import Foundation
import UIKit.UINavigationController

// MARK: - ARCHITECTURE PROTOCOLS
protocol ViewToPresenterHomeProtocol: AnyObject {
    var view: PresenterToViewHomeProtocol? {get set}
    var interactor: PresenterToInteractorHomeProtocol? {get set}
    var router: PresenterToRouterHomeProtocol? {get set}
    
    func fetchTVShows()
    func fetchTVShowDetail(index: Int)
    func fetchFavorites()
    func addTVShowToFavorites(index: Int)
    func deleteTVShowFromFavorites(id: String, index: Int, isHomeSelected: Bool)
    func showTVShowDetail(_ navigationController: UINavigationController?, tvShowDetail: TVShowDetail)
}

protocol PresenterToViewHomeProtocol: AnyObject {
    func showHomeData(tvShowList: [TVShowList])
    func showTVShowDetailScreen(tvShowDetail: TVShowDetail)
    func showFavorites(favoriteList: [TVShowFavorite])
    func updateTVShowList(row: Int, tvShowList: [TVShowList])
    func showAlertError(errorAlert: HomeErrorAlerts)
}

protocol PresenterToRouterHomeProtocol: AnyObject {
    func pushToTVShowDetail(_ navigationController: UINavigationController?, tvShowDetail: TVShowDetail)
}

protocol PresenterToInteractorHomeProtocol: AnyObject {
    var presenter: InteractorToPresenterHomeProtocol? {get set}
    func fetchTVShows()
    func fetchTVShowDetail(index: Int)
    func fetchFavorites()
    func saveTVShowAsFavorite(index: Int)
    func deleteTVShowAsFavorite(id: String, index: Int, isHomeSelected: Bool)
}

protocol InteractorToPresenterHomeProtocol: AnyObject {
    func tvShowsListFetched(tvShowList: [TVShowList])
    func returnTVShowDetail(tvShowDetail: TVShowDetail)
    func favoritesListFetched(favoriteList: [TVShowFavorite])
    func tvShowListUpdated(_ row: Int, tvShowList: [TVShowList])
    func tvShowsListFailed()
    func tvShowSaveAsFavorite(_ success: Bool)
    func tvShowDeleteAsFavorite(_ success: Bool)
    func errorServer()
}
