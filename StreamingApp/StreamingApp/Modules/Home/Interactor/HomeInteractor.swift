//
//  HomeInteractor.swift
//  StreamingApp
//
//  Created by José Ángel López on 23/02/22.
//

import Foundation

class HomeInteractor: PresenterToInteractorHomeProtocol {
    var presenter: InteractorToPresenterHomeProtocol?
    private var tvShowList = [TVShowList]()
    private var tvShowDetailList = [TVShowDetail]()
  
    // MARK: - HTTP REQUEST METHODS
    func fetchTVShows() {
        let httpRequest = HTTPRequest()
        guard let url = HTTPInvoker().getAllShows() else { return }
        httpRequest.getData(url: url, httpMethod: .get, completion: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let data):
                if let data = data, let tvShows = try? JSONDecoder().decode([TVShows].self, from: data) {
                    self.createTVShowsList(tvShows: tvShows)
                    self.presenter?.tvShowsListFetched(tvShowList: self.tvShowList)
                }
            case .failure(let networkError):
                if networkError == .serviceError {
                    self.presenter?.tvShowsListFailed()
                } else if networkError == .serverError {
                    self.presenter?.errorServer()
                }
            }
        })
    }
    
    // MARK: - DATABASE REQUEST METHODS
    func fetchFavorites() {
        let coreDataManager = CoreDataManager()
        let favoriteList = coreDataManager.fetchTVShows()
        var newFavoriteList = [TVShowFavorite]()
        for favorite in favoriteList {
            newFavoriteList.append(TVShowFavorite(id: favorite.id, name: favorite.name, imageURL: favorite.imageURL))
        }
        presenter?.favoritesListFetched(favoriteList: newFavoriteList)
    }
    
    func saveTVShowAsFavorite(index: Int) {
        let coreDataManager = CoreDataManager()
        coreDataManager.saveTVShow(tvShow: tvShowList[index], completion: { [weak self] success in
            if success {
                self?.updateTVShow()
                self?.presenter?.tvShowListUpdated(index, tvShowList: self?.tvShowList ?? [])
            } else {
                self?.presenter?.tvShowSaveAsFavorite(success)
            }
        })
    }
    
    func deleteTVShowAsFavorite(id: String, index: Int, isHomeSelected: Bool) {
        let coreDataManager = CoreDataManager()
        coreDataManager.deleteTVShow(id: id, completion: { [weak self] success in
            if success {
                if isHomeSelected {
                    self?.updateTVShow()
                    self?.presenter?.tvShowListUpdated(index, tvShowList: self?.tvShowList ?? [])
                } else {
                    self?.fetchFavorites()
                }
            } else {
                self?.presenter?.tvShowDeleteAsFavorite(success)
            }
        })
    }
    
    // MARK: - BUILD METHODS
    func fetchTVShowDetail(index: Int) {
        let tvShowDetail = returnTVShowDetail(index: index)
        presenter?.returnTVShowDetail(tvShowDetail: tvShowDetail)
    }
    
    private func createTVShowsList(tvShows: [TVShows]) {
        var tvShowList = [TVShowList]()
        var tvShowDetailList = [TVShowDetail]()
        for tvShow in tvShows {
            tvShowList.append(TVShowList(id: tvShow.id, name: tvShow.name, imageURL: tvShow.image?.medium))
            tvShowDetailList.append(TVShowDetail(id: tvShow.id, name: tvShow.name, image: tvShow.image?.original, summary: tvShow.summary, imdb: tvShow.externals?.imdb))
        }
        self.tvShowList = tvShowList
        self.tvShowDetailList = tvShowDetailList
        updateTVShow()
    }
    
    private func returnTVShowDetail(index: Int) -> TVShowDetail {
        return tvShowDetailList[index]
    }
    
    private func updateTVShow() {
        let coreDataManager = CoreDataManager()
        let favoriteList = coreDataManager.fetchTVShows()
        self.tvShowList = tvShowList.map({
            var dict = $0
            dict.isFavorite = false
            return dict
        })
        for (index, tvShow) in tvShowList.enumerated() {
            for favorite in favoriteList {
                if let id = tvShow.id, "\(id)" == favorite.id {
                    self.tvShowList[index].isFavorite = true
                }
            }
        }
    }
}
