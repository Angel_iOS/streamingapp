//
//  TVShowViewCell.swift
//  StreamingApp
//
//  Created by José Ángel López on 25/02/22.
//

import UIKit

class TVShowViewCell: UITableViewCell {
    
    // MARK: - OUTLETS
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tvShowImage: UIImageView!
    
    func setFields(_ tvShow: TVShowList) {
        titleLabel.text = tvShow.name
        tvShowImage.load(url: URL(string: tvShow.imageURL ?? ""))
    }
    
    func setFavorites(_ favorite: TVShowFavorite) {
        titleLabel.text = favorite.name
        tvShowImage.load(url: URL(string: favorite.imageURL ?? ""))
    }
}
