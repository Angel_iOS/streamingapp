//
//  ShowsViewController.swift
//  StreamingApp
//
//  Created by José Ángel López on 23/02/22.
//

import UIKit

class HomeViewController: UIViewController {
    // MARK: - OUTLETS
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - VARIABLES
    private var tvShowList: [TVShowList] = []
    private var favoriteList: [TVShowFavorite] = []
    var homePresenter: ViewToPresenterHomeProtocol?
    var isHomeSelected = true
    
    // MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewComponents()
        loadInitData()
    }
    
    // MARK: - LOAD DATA
    private func loadInitData() {
        createActivityIndicator()
        if isHomeSelected {
            homePresenter?.fetchTVShows()
        }
    }
    
    // MARK: - SETUP VIEWCOMPONENTS
    private func setupViewComponents() {
        registerCell()
        setTableView()
    }
    
    private func registerCell() {
        tableView.register(UINib(nibName: TVShowViewCell.identifier, bundle: nil), forCellReuseIdentifier: TVShowViewCell.identifier )
    }
    
    private func setTableView() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    // MARK: - USER INTERACTION
    private func perfomSwipeAction(id: String?, index: Int, isFavorite: Bool) {
        if isFavorite {
            showAlert(Constants.Alerts.deleteTVShowQuestion, action: { [self] in
                guard let id = id else { return }
                self.homePresenter?.deleteTVShowFromFavorites(id: id, index: index, isHomeSelected: self.isHomeSelected)
            })
        } else {
            homePresenter?.addTVShowToFavorites(index: index)
        }
    }
    
    func showHomeData(_ state: Bool) {
        isHomeSelected = state
        isHomeSelected ? homePresenter?.fetchTVShows() : homePresenter?.fetchFavorites()
    }
}

// MARK: - ARCHITECTURE PROTOCOLS
extension HomeViewController: PresenterToViewHomeProtocol {
    func showHomeData(tvShowList: [TVShowList]) {
        self.tvShowList = tvShowList
        if self.tvShowList.count == 0 { showAlert(Constants.Alerts.tvShowListIsEmpty) }
        tableView.reloadData()
        self.activityStopAnimating()
    }
    
    func showTVShowDetailScreen(tvShowDetail: TVShowDetail) {
        self.homePresenter?.showTVShowDetail(self.navigationController, tvShowDetail: tvShowDetail)
    }
    
    func showFavorites(favoriteList: [TVShowFavorite]) {
        self.favoriteList = favoriteList
        if self.favoriteList.count == 0 { showAlert(Constants.Alerts.favoritesListIsEmpty) }
        tableView.reloadData()
        self.activityStopAnimating()
    }
    
    func updateTVShowList(row: Int, tvShowList: [TVShowList]) {
        self.tvShowList = tvShowList
        tableView.reloadRows(at: [IndexPath(row: row, section: 0)], with: .fade)
    }
    
    func showAlertError(errorAlert: HomeErrorAlerts) {
        self.showAlert(errorAlert.message)
        self.activityStopAnimating()
    }
}

// MARK: - TABLE VIEW DATA SOURCE METHODS
extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isHomeSelected ? tvShowList.count : favoriteList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TVShowViewCell.identifier, for: indexPath) as? TVShowViewCell else {
            return UITableViewCell()
        }
        if isHomeSelected {
            cell.setFields(tvShowList[indexPath.row])
        } else {
            cell.setFavorites(favoriteList[indexPath.row])
        }
        cell.selectionStyle = .none
        cell.accessoryType = isHomeSelected ? .disclosureIndicator : .none
        return cell
    }
}

// MARK: - TABLE VIEW DELEGATE METHODS
extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if isHomeSelected {
            let isFavorite = tvShowList[indexPath.row].isFavorite
            let id = "\(self.tvShowList[indexPath.row].id ?? 0)"
            let action = UIContextualAction(style: .destructive, title: isFavorite ? Constants.SwipeActionTitle.delete : Constants.SwipeActionTitle.favorite) { [weak self] (action, view, completionHandler) in
                self?.perfomSwipeAction(id: id, index: indexPath.row, isFavorite: isFavorite)
                completionHandler(true)
            }
            action.backgroundColor = isFavorite ? .systemRed : .systemGreen
            return UISwipeActionsConfiguration(actions: [action])
        } else {
            let id = self.favoriteList[indexPath.row].id ?? ""
            let action = UIContextualAction(style: .destructive, title: Constants.SwipeActionTitle.delete) { [weak self] (action, view, completionHandler) in
                self?.perfomSwipeAction(id: id, index: indexPath.row, isFavorite: true)
                completionHandler(true)
            }
            action.backgroundColor = .systemRed
            return UISwipeActionsConfiguration(actions: [action])
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isHomeSelected ? homePresenter?.fetchTVShowDetail(index: indexPath.row) : ()
    }
}
