//
//  HomePresenter.swift
//  StreamingApp
//
//  Created by José Ángel López on 23/02/22.
//

import Foundation
import UIKit.UINavigationController

class HomePresenter: ViewToPresenterHomeProtocol {
    var view: PresenterToViewHomeProtocol?
    var interactor: PresenterToInteractorHomeProtocol?
    var router: PresenterToRouterHomeProtocol?
    
    func fetchTVShows() {
        interactor?.fetchTVShows()
    }
    
    func fetchTVShowDetail(index: Int) {
        interactor?.fetchTVShowDetail(index: index)
    }
    
    func fetchFavorites() {
        interactor?.fetchFavorites()
    }
    
    func addTVShowToFavorites(index: Int) {
        interactor?.saveTVShowAsFavorite(index: index)
    }
    
    func deleteTVShowFromFavorites(id: String, index: Int, isHomeSelected: Bool) {
        interactor?.deleteTVShowAsFavorite(id: id, index: index, isHomeSelected: isHomeSelected)
    }
    
    func showTVShowDetail(_ navigationController: UINavigationController?, tvShowDetail: TVShowDetail) {
        router?.pushToTVShowDetail(navigationController, tvShowDetail: tvShowDetail)
    }
}

// MARK: - ARCHITECTURE EXTENSION
extension HomePresenter: InteractorToPresenterHomeProtocol {
    func tvShowsListFetched(tvShowList: [TVShowList]) {
        view?.showHomeData(tvShowList: tvShowList)
    }
    
    func returnTVShowDetail(tvShowDetail: TVShowDetail) {
        view?.showTVShowDetailScreen(tvShowDetail: tvShowDetail)
    }
    
    func favoritesListFetched(favoriteList: [TVShowFavorite]) {
        view?.showFavorites(favoriteList: favoriteList)
    }
    
    func tvShowListUpdated(_ row: Int, tvShowList: [TVShowList]) {
        view?.updateTVShowList(row: row, tvShowList: tvShowList)
    }
    
    func tvShowSaveAsFavorite(_ success: Bool) {
        if !success {
            view?.showAlertError(errorAlert: .saveTVShowAsFavoriteFailed)
        }
    }
    
    func tvShowDeleteAsFavorite(_ success: Bool) {
        if !success {
            view?.showAlertError(errorAlert: .deleteTVShowAsFavoriteFailed)
        }
    }
    
    func tvShowsListFailed() {
        view?.showAlertError(errorAlert: .fetchTVShowsFailed)
    }
    
    func errorServer() {
        view?.showAlertError(errorAlert: .serverFailed)
    }
}
