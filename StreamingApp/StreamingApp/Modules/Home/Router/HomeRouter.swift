//
//  HomeRouter.swift
//  StreamingApp
//
//  Created by José Ángel López on 23/02/22.
//

import Foundation
import UIKit.UINavigationController

class HomeRouter: PresenterToRouterHomeProtocol {
    static func createShowsScreen() -> HomeViewController {
        let view = HomeViewController(nibName: Constants.XIBName.shows, bundle: nil)
        let presenter: ViewToPresenterHomeProtocol & InteractorToPresenterHomeProtocol = HomePresenter()
        let interactor: PresenterToInteractorHomeProtocol = HomeInteractor()
        let router: PresenterToRouterHomeProtocol = HomeRouter()
        
        view.homePresenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return view
    }
    
    func pushToTVShowDetail(_ navigationController: UINavigationController?, tvShowDetail: TVShowDetail) {
        let viewController = DetailRouter.createDetailScreen(tvShowDetail: tvShowDetail)
        navigationController?.pushViewController(viewController, animated: true)
    }
}
