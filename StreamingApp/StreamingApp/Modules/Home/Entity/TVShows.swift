//
//  TVShows.swift
//  StreamingApp
//
//  Created by José Ángel López on 24/02/22.
//

import Foundation

struct TVShows: Codable {
    let id: Int?
    let url: String?
    let name: String?
    let type: String?
    let language: String?
    let genres: [String]?
    let status: String?
    let runtime: Int?
    let averageRuntime: Int?
    let premiered: String?
    let ended: String?
    let officialSite: String?
    let schedule: Schedule?
    let rating: Rating?
    let weight: Float?
    let webChannel: WebChannel?
    let dvdCountry: DvdCountry?
    let externals: Externals?
    let image: Image?
    let summary: String?
    let updated: Int?
    
    struct Schedule: Codable {
        let time: String?
        let days: [String]?
    }
    
    struct Rating: Codable {
        let average: Float?
    }
    
    struct Network: Codable {
        let id: Int?
        let name: String?
    }
    
    struct WebChannel: Codable {
        let id: Int?
        let name: String?
    }
    
    struct DvdCountry: Codable {
        let name: String?
        let code: String?
        let timezone: String?
    }
    
    struct Externals: Codable {
        let tvrage: Int?
        let thetvdb: Int?
        let imdb: String?
    }
    
    struct Image: Codable {
        let medium: String?
        let original: String?
    }
}

struct TVShowList {
    let id: Int?
    let name: String?
    let imageURL: String?
    var isFavorite: Bool = false
}

struct TVShowFavorite {
    let id: String?
    let name: String?
    let imageURL: String?
}
