//
//  HTTPInvoker.swift
//  StreamingApp
//
//  Created by José Ángel López on 24/02/22.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

enum NetworkError: Error {
    case serviceError
    case serverError
}

private let baseURL: String = "http://api.tvmaze.com"

class HTTPInvoker {
    public func getAllShows() -> URL? {
        return URL(string: "\(baseURL)/shows")
    }
}
