//
//  HTTPRequest.swift
//  StreamingApp
//
//  Created by José Ángel López on 24/02/22.
//

import Foundation

class HTTPRequest {
func getData(url: URL, httpMethod: HTTPMethod, completion: @escaping (Result<Data?, NetworkError>) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {  data, response, _ in
            DispatchQueue.main.async {
                if let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    completion(.success(data))
                } else if let response = response as? HTTPURLResponse, response.statusCode == 500 {
                    completion(.failure(.serverError))
                } else {
                    completion(.failure(.serviceError))
                }
            }
        })
        task.resume()
    }
}
