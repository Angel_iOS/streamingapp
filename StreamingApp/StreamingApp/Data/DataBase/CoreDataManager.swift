//
//  CoreDataManager.swift
//  StreamingApp
//
//  Created by José Ángel López on 27/02/22.
//

import Foundation
import CoreData
import UIKit

class CoreDataManager {
    var container: NSPersistentContainer
    
    init() {
        container = NSPersistentContainer(name: Constants.Database.name)
        setupDatabase()
    }
    
    private func setupDatabase() {
        container.loadPersistentStores(completionHandler: {_,_ in })
    }
    
    func saveTVShow(tvShow: TVShowList, completion: @escaping(Bool) -> Void) {
        let context = container.viewContext
        
        let favorite = Favorite(context: context)
        favorite.id = "\(tvShow.id ?? 0)"
        favorite.name = tvShow.name
        favorite.imageURL = tvShow.imageURL
        do {
            try context.save()
            completion(true)
        } catch {
            completion(false)
        }
    }
    
    func fetchTVShows() -> [Favorite] {
        let fetchRequest: NSFetchRequest<Favorite> = Favorite.fetchRequest()
        do {
            let result = try container.viewContext.fetch(fetchRequest)
            return result
        } catch {
            return []
        }
    }
    
    func deleteTVShow(id: String, completion: @escaping(Bool) -> Void) {
        let fetchRequest: NSFetchRequest<Favorite> = Favorite.fetchRequest()
        do {
            let items = try container.viewContext.fetch(fetchRequest)
            for item in items where item.id == id {
                container.viewContext.delete(item)
                print("Elemento eliminado")
            }
            try container.viewContext.save()
            completion(true)
        } catch {
            completion(false)
        }
    }
}
