//
//  Extension+UIViewController.swift
//  StreamingApp
//
//  Created by José Ángel López on 25/02/22.
//

import UIKit.UIViewController

extension UIViewController {
    func showAlert(_ message: String) {
        let alert = UIAlertController(title: Constants.Alerts.title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.Alerts.buttonDoneTitle, style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    func showAlert(_ message: String, action: @escaping () -> Void) {
        let alert = UIAlertController(title: Constants.Alerts.title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.Alerts.buttonDoneTitle,style: .default,handler: {_ in
            alert.dismiss(animated: true, completion: action)
        }))
        alert.addAction(UIAlertAction(title: Constants.Alerts.buttonCancelTitle, style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func createActivityIndicator() {
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .large
        activityIndicator.color = UIColor().getPurpleCustomColor()
        activityIndicator.startAnimating()
        self.view.isUserInteractionEnabled = false
        self.view.addSubview(activityIndicator)
    }
    
    func activityStopAnimating() {
        for view in self.view.subviews {
            if view.isKind(of: UIActivityIndicatorView.self) {
                view.removeFromSuperview()
                self.view.isUserInteractionEnabled = true
            }
        }
    }
    
}
