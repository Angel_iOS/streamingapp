//
//  Extension+UITableViewCell.swift
//  StreamingApp
//
//  Created by José Ángel López on 25/02/22.
//

import UIKit.UITableViewCell

extension UITableViewCell {
    class var identifier: String { return String(describing: self) }
}
