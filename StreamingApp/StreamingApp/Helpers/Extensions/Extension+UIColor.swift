//
//  Extension+UIColor.swift
//  StreamingApp
//
//  Created by José Ángel López on 26/02/22.
//

import UIKit.UIColor

extension UIColor{
    func getPurpleCustomColor() -> UIColor {
        return UIColor(red: 0.4, green: 0.12, blue: 1, alpha: 1)
    }
}
