//
//  Extension+UIImageView.swift
//  StreamingApp
//
//  Created by José Ángel López on 25/02/22.
//

import UIKit.UIImageView

let cache = NSCache<AnyObject, UIImage>()

extension UIImageView {
    func load(url: URL?) {
        guard let url = url else { return }
        if let cachedImage = cache.object(forKey: url as AnyObject) {
            self.image = cachedImage
            return
        }
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: url) {
                DispatchQueue.main.async {
                    self.image = UIImage(data: data)
                }
            }
        }
    }
}
