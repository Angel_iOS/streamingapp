//
//  Alerts.swift
//  StreamingApp
//
//  Created by José Ángel López on 01/03/22.
//

import Foundation

enum HomeErrorAlerts: String {
    case fetchTVShowsFailed
    case serverFailed
    case saveTVShowAsFavoriteFailed
    case deleteTVShowAsFavoriteFailed
    
    var message: String {
        switch self {
        case .fetchTVShowsFailed, .serverFailed:
            return Constants.Alerts.serviceError
        case .saveTVShowAsFavoriteFailed:
            return Constants.Alerts.saveTVShowAsFavoriteFailed
        case .deleteTVShowAsFavoriteFailed:
            return Constants.Alerts.deleteTVShowAsFavoriteFailed
        }
    }
}
