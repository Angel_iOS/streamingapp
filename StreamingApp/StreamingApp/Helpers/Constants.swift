//
//  Constants.swift
//  StreamingApp
//
//  Created by José Ángel López on 23/02/22.
//

import Foundation

struct Constants {
    struct navigationBarTitle {
        static let home = "TV Shows"
        static let favorites = "Favorites"
    }
    
    struct XIBName {
        static let dashboard: String = "DashboardView"
        static let shows: String = "HomeView"
        static let favorites: String = "HomeView"
        static let detail: String = "DetailView"
    }
    
    struct tabBarImages {
        static let shows = "shows"
        static let favorites = "favorites"
    }
    
    struct tabBarTitles {
        static let shows = "Shows"
        static let favorites = "Favorites"
    }
    
    struct Alerts {
        static let title = "Streaming App"
        static let buttonDoneTitle = "Accept"
        static let buttonCancelTitle = "Cancel"
        static let tvShowListIsEmpty = "No TV Shows to show."
        static let favoritesListIsEmpty = "No Favorites to show."
        static let saveTVShowAsFavoriteFailed = "There was a problem saving this TV Show. Do you want to try again?"
        static let deleteTVShowAsFavoriteFailed = "There was a problem saving this TV Show. Do you want to try again?"
        static let serviceError = "An error occurred while fetching data. Do you want to try again?"
        static let deleteTVShowQuestion = "Do you want delete TV Show from favorites?"
    }
    
    struct SwipeActionTitle {
        static let favorite = "Favorites"
        static let delete = "Delete"
    }
    
    struct Database {
        static let name = "StreamingApp"
    }
 }
